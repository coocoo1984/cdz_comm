package com.zkzl;

import com.zkzl.cdz.netty.NettyServerListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CdzApplication implements CommandLineRunner {
    public static void main(String[] args) {
        SpringApplication.run(CdzApplication.class, args);
    }

    @Autowired
    NettyServerListener listener;

    @Override
    public void run(String... args) throws Exception {
        listener.start();
    }


}