package com.zkzl.cdz.util;

import java.util.UUID;

/**
 * @program: NettyTEST
 * @description: 工具类
 * @author: QSY
 * @create: 2019-07-22 14:47
 **/
public class Tools {

    private static int data;
    private static int length;

    /**
     * CRC16校验
     *
     * @param arr_buff
     * @return
     */
    public static byte[] CRC16(byte[] arr_buff) {
        int len = arr_buff.length;

        // 预置 1 个 16 位的寄存器为十六进制FFFF, 称此寄存器为 CRC寄存器。
        int crc = 0xFFFF;
        int i, j;
        for (i = 0; i < len; i++) {
            // 把第一个 8 位二进制数据 与 16 位的 CRC寄存器的低 8 位相异或, 把结果放于 CRC寄存器
            crc = ((crc & 0xFF00) | (crc & 0x00FF) ^ (arr_buff[i] & 0xFF));
            for (j = 0; j < 8; j++) {
                // 把 CRC 寄存器的内容右移一位( 朝低位)用 0 填补最高位, 并检查右移后的移出位
                if ((crc & 0x0001) > 0) {
                    // 如果移出位为 1, CRC寄存器与多项式A001进行异或
                    crc = crc >> 1;
                    crc = crc ^ 0xA001;
                } else
                    // 如果移出位为 0,再次右移一位
                    crc = crc >> 1;
            }
        }
        //低字节在前，高字节在后
        byte[] src = new byte[2];
        src[1] = (byte) ((crc >> 8) & 0xFF);
        src[0] = (byte) (crc & 0xFF);
        return src;
    }

    /**
     * 将整形数据转换为字节型数据，高字节在前，低字节在后
     *
     * @param data
     * @param length
     * @return
     */
    public static byte[] IntConvertByteHL(int data, int length) {
        byte[] b = new byte[4];
        b[0] = (byte) ((data >> 24) & 0xff);
        b[1] = (byte) ((data >> 16) & 0xff);
        b[2] = (byte) ((data >> 8) & 0xff);
        b[3] = (byte) (data & 0xff);
        byte[] res = new byte[length];
        for (int index = 0; index < length; index++) {
            res[index] = b[4 - length + index];
        }
        return res;
    }

    /**
     * 将字节数组转换为整形输出
     *
     * @param data
     * @return
     */
    public static int ByteConvertIntHL(byte[] data) {
        int intValue = 0;
        for (int i = 0; i < data.length; i++) {
            intValue += (data[i] & 0xFF) << (8 * (data.length - i - 1));
        }
        return intValue;
    }

    public static int ByteConvertIntHL(byte data) {
        int intValue = 0;
        intValue += (data & 0xFF) ;
        return intValue;
    }

    /**
     * 将ascii的字节数组转换成字符串
     *
     * @param data
     * @return
     */
    public static String ASCIIConvertStr(byte[] data) {
        StringBuilder sb = new StringBuilder();
        for (byte x : data) {
            sb.append((char) x);
        }
        return sb.toString();
    }

    /**
     * 将ascii字符串转成ascii的字节数组
     *
     * @param data
     * @return
     */
    public static byte[] StronvertASCII(String data) {
        char[] c = data.toCharArray();
        byte[] b = new byte[c.length];
        for (int i = 0; i < c.length; i++) {
            b[i] = (byte) c[i];
        }
        return b;
    }

    /**
     * 生成id
     *
     * @return
     */
    public static String makeId() {
        UUID uuid = UUID.randomUUID();
        String str = uuid.toString();
        String uuidStr = str.replace("-", "");
        return uuidStr;
    }

    private final static String[] hexArray = {"0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "A", "B", "C", "D", "E", "F"};

    public static  String byteToHex(int n) {
        if (n < 0) {
            n = n + 256;
        }
        int d1 = n / 16;
        int d2 = n % 16;
        return hexArray[d1] + hexArray[d2];
    }
}
