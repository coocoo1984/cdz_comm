package com.zkzl.cdz.netty;


import redis.clients.jedis.Jedis;

import java.util.Vector;

/**
 * @program: NettyTEST
 * @description: Redis连接池
 * @author: QSY
 * @create: 2019-07-24 18:44
 **/
public class JRedisPool {
    //创建连接池配置对象：
    private static Vector<Jedis> jedisVector = new Vector<Jedis>(10);

    static {
        for (int i = 0; i < 10; i++) {
            jedisVector.add(new Jedis("localhost"));
        }
    }

    public static void SetHashVal(String key, String fid, String val, int database, int second) {
        Jedis jredis = null;
        if (jedisVector.size() <= 0) {
            jredis = new Jedis("localhost");
        } else {
            jredis = jedisVector.get(0);
            jedisVector.remove(0);
        }
        if (jredis != null) {
            try {
                jredis.auth("zkzl1-1=redis");
                jredis.select(database);
                jredis.hset(key, fid, val);
                jredis.expire(key, second);
                if (jedisVector.size() >= 10) {
                    jedisVector.add(jredis);
                } else {
                    jredis.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String GetHashVal(String key, String fid, int database) {
        String val = null;
        Jedis jredis = null;
        if (jedisVector.size() <= 0) {
            jredis = new Jedis("localhost");
        } else {
            jredis = jedisVector.get(0);
            jedisVector.remove(0);
        }
        try {
            jredis.auth("zkzl1-1=redis");
            jredis.select(database);
            val = jredis.hget(key, fid);
            if (jedisVector.size() >= 10) {
                jedisVector.add(jredis);
            } else {
                jredis.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return val;
    }

    public static void setKV(String key, String val, int database) {
        Jedis jredis = null;
        if (jedisVector.size() <= 0) {
            jredis = new Jedis("localhost");
        } else {
            jredis = jedisVector.get(0);
            jedisVector.remove(0);
        }
        if (jredis != null) {
            try {
                jredis.auth("zkzl1-1=redis");
                jredis.select(database);
                jredis.set(key, val);
                if (jedisVector.size() >= 10) {
                    jedisVector.add(jredis);
                } else {
                    jredis.close();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static String getKV(String key, int database) {
        String val = null;
        Jedis jredis = null;
        if (jedisVector.size() <= 0) {
            jredis = new Jedis("localhost");
        } else {
            jredis = jedisVector.get(0);
            jedisVector.remove(0);
        }
        try {
            jredis.auth("zkzl1-1=redis");
            jredis.select(database);
            val = jredis.get(key);
            if (jedisVector.size() >= 10) {
                jedisVector.add(jredis);
            } else {
                jredis.close();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return val;
    }




}
