package com.zkzl.cdz.netty;

import org.springframework.stereotype.Component;

/**
 * @Description: Netty服务器常量
 * @Author: jww
 * @CreateDate: 2019/8/7 11:50
 * @Version: 1.0.0
 */
@Component
public class NettyConstant {

    /**
     * 最大线程量
     */
    private static final int MAX_THREADS = 1024;
    /**
     * 数据包最大长度
     */
    private static final int MAX_FRAME_LENGTH = 65535;

    public static int getMaxFrameLength() {
        return MAX_FRAME_LENGTH;
    }

    public static int getMaxThreads() {
        return MAX_THREADS;
    }
}
