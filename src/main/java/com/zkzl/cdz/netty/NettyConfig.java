package com.zkzl.cdz.netty;

import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @Description: 读取yml配置文件中的信息
 * @Author: jww
 * @CreateDate: 2019/8/7 11:51
 * @Version: 1.0.0
 */

@Data
@Configuration
@ConfigurationProperties(prefix = "netty")
public class NettyConfig {
    private int port;
}
