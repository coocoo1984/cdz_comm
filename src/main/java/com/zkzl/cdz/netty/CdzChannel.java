package com.zkzl.cdz.netty;

import io.netty.channel.Channel;
import io.netty.channel.ChannelId;
import io.netty.channel.group.ChannelGroup;
import io.netty.channel.group.DefaultChannelGroup;
import io.netty.util.concurrent.GlobalEventExecutor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.util.CollectionUtils;

/**
 * @Description: 充电桩channel管理
 * @Author: jww
 * @CreateDate: 2019/8/10 15:04
 * @Version: 1.0.0
 */
@Slf4j
public class CdzChannel {

    public static ChannelGroup CHANNEL_GROUP = new DefaultChannelGroup("ChannelGroups", GlobalEventExecutor.INSTANCE);


    public static Channel getChannel(ChannelId channelId) {
        synchronized (CHANNEL_GROUP) {
            // 初始化
            if (CollectionUtils.isEmpty(CHANNEL_GROUP)) {
                log.info("无CHANEL信息");
                return null;
            }
            return CHANNEL_GROUP.find(channelId);
        }
    }


    public static void removeChannel(Channel channel) {
        synchronized (CHANNEL_GROUP) {
            CHANNEL_GROUP.remove(channel);
        }
    }

    public static void addChannel(Channel channel) {
        synchronized (CHANNEL_GROUP) {
            CHANNEL_GROUP.add(channel);
        }
    }


}
