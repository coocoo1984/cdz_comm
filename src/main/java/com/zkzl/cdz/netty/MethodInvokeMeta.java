package com.zkzl.cdz.netty;

import lombok.Data;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * @Description: 记录调用方法的元信息
 * @Author: jww
 * @CreateDate: 2019/8/7 12:01
 * @Version: 1.0.0
 */
@Data
@Component
public class MethodInvokeMeta implements Serializable {
    private static final long serialVersionUID = 8379109667714148890L;
    //接口
    private Class<?> interfaceClass;
    //方法名
    private String methodName;
    //参数
    private Object[] args;
    //返回值类型
    private Class<?> returnType;
    //参数类型
    private Class<?>[] parameterTypes;

}