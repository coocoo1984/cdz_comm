package com.zkzl.cdz.handler;

import com.zkzl.cdz.util.Tools;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelOutboundHandlerAdapter;
import io.netty.channel.ChannelPromise;
import lombok.extern.slf4j.Slf4j;

/**
 * @program: NettyTEST
 * @description: 发送数据
 * @author: QSY
 * @create: 2019-07-24 08:57
 **/

@Slf4j
public class ResponseEncoder extends ChannelOutboundHandlerAdapter {
    @Override
    public void write(ChannelHandlerContext ctx, Object msg, ChannelPromise promise) throws Exception {
        byte[] mess = (byte[]) msg;
        ByteBuf encoded = ctx.alloc().buffer(mess.length);
        encoded.writeBytes(mess);
        ctx.write(encoded);
        ctx.flush();
        StringBuilder sb = new StringBuilder();
        for (byte aa : mess) {
            sb.append(Integer.toHexString(Tools.ByteConvertIntHL(aa)) + "\t");
        }
        log.info("回复数据： " + sb.toString());
    }
}
