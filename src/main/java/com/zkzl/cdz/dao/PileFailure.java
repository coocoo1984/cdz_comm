package com.zkzl.cdz.dao;

import lombok.Data;

import java.util.Date;

@Data
public class PileFailure {
    private String id;
    private String code;
    private String cause;
    private Integer causeId;
    private Date time;


}
