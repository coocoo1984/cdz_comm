package com.zkzl.cdz.dao;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;
import java.util.Map;

@Mapper
public interface SysDao {
    int replacePileSession(PileSession pileSession);

    ChargeStation findStationBySessionId(@Param("sessionId") String sessionId);

    PileSession findPileSession(PileSession pileSession);

    int batchSavePileFailure(List<PileFailure> pileFailure);

    void callAutoPile(Map<String, Object> param);

    int flushChargingPile(ChargingPile chargingPile);

    List<Order> findOrderNoByMap(Map<String, Object> map);

    void callErrorState3(Map<String, Object> param);

    ChargeStation findStationMsg(@Param("code") String code);

}
