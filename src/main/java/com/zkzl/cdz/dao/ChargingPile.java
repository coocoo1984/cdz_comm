package com.zkzl.cdz.dao;

import lombok.Data;

@Data
public class ChargingPile {
    private String state;
    private String sn;
}
