package com.zkzl.cdz.dao;

import lombok.Data;

import java.util.Date;

@Data
public class Order {

    private String orderNo;

    private Date createTime;
}
