package com.zkzl.cdz.dao;

import lombok.Data;

/**
 * @program: NettyTEST
 * @description: 桩信息类
 * @author: QSY
 * @create: 2019-07-24 18:34
 **/
@Data
public class Pile {
    private String sessionId;//桩sessionid
    private int macstate;//充电机状态
    private int gunstate;//枪状态
    private int usedPower;//枪已充电量
    private int usedTime;//枪已充电时间
    //public int remainingTime ;
    // 枪估计剩余时间
    private int eFee;//当前电费
    private int sFee;//当前服务费
    private String manufacturers;//厂家
    private int soc;//当前电池容量,交流桩不存在该值
    private String date;//记录的时间
    private int delay;//网路延迟时间
}
