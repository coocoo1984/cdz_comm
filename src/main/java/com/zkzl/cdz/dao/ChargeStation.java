package com.zkzl.cdz.dao;

import lombok.Data;

@Data
public class ChargeStation {


    private int servicePrice;

    private int electricFee;

    private String name;

    private String address;
}
