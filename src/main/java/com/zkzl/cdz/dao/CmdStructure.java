package com.zkzl.cdz.dao;

import com.zkzl.cdz.util.Tools;

/**
 * @program: NettyTEST
 * @description: 指令结构
 * @author: QSY
 * @create: 2019-07-22 18:08
 **/
public class CmdStructure {

    public CmdStructure() {
        serial = 0;
        gun = 0;
        start = new byte[]{0x55, (byte) 0xAA};
        end = 0x0D;
    }

    public byte[] start;//帧起始域
    public byte[] leng;//帧长度域
    public byte[] lengback;//帧长度域取反
    public byte serial;//序列号域
    public byte gun;//枪序号，0表示全局
    public byte type;//指令类型
    public byte[] body;//数据域
    public byte[] check;//帧校验域
    public byte end;//帧结束域

    public byte[] ToBytes(CmdStructure cmdStructure) {
        if (body == null || body.length == 0) {
            return toArrayNoBody(cmdStructure);
        } else {
            return toArrayWithBody(cmdStructure);
        }
    }

    private byte[] toArrayWithBody(CmdStructure command_) {
        command_.leng = Tools.IntConvertByteHL(5 + body.length, 2);
        command_.lengback = new byte[2];
        command_.lengback[0] = (byte) ~command_.leng[0];
        command_.lengback[1] = (byte) ~command_.leng[1];
        byte[] crcdata = new byte[3 + command_.body.length];
        crcdata[0] = command_.serial;
        crcdata[1] = command_.gun;
        crcdata[2] = command_.type;
        System.arraycopy(command_.body, 0, crcdata, 3, command_.body.length);
        command_.check = Tools.CRC16(crcdata);
        byte[] result = new byte[12 + command_.body.length];
        System.arraycopy(command_.start, 0, result, 0, 2);
        System.arraycopy(command_.leng, 0, result, 2, 2);
        System.arraycopy(command_.lengback, 0, result, 4, 2);
        result[6] = command_.serial;
        result[7] = command_.gun;
        result[8] = command_.type;
        System.arraycopy(command_.body, 0, result, 9, command_.body.length);
        System.arraycopy(command_.check, 0, result, command_.body.length + 9, 2);
        result[command_.body.length + 11] = command_.end;
        return result;
    }

    private byte[] toArrayNoBody(CmdStructure command_) {
        command_.leng = Tools.IntConvertByteHL(5, 2);
        command_.lengback = new byte[2];
        command_.lengback[0] = (byte) ~command_.leng[0];
        command_.lengback[1] = (byte) ~command_.leng[1];
        byte[] crcdata = new byte[3];
        crcdata[0] = command_.serial;
        crcdata[1] = command_.gun;
        crcdata[2] = command_.type;
        command_.check = Tools.CRC16(crcdata);
        byte[] result = new byte[12];
        System.arraycopy(command_.start, 0, result, 0, 2);
        System.arraycopy(command_.leng, 0, result, 2, 2);
        System.arraycopy(command_.lengback, 0, result, 4, 2);
        result[6] = command_.serial;
        result[7] = command_.gun;
        result[8] = command_.type;
        System.arraycopy(command_.check, 0, result, 9, 2);
        result[11] = command_.end;
        return result;
    }

    @Override
    public String toString() {
        return "CmdStructure{"
                + " 枪号="
                + gun
                + ", 指令类型=0X"
                + Tools.byteToHex(type)
                + '}';
    }
}
