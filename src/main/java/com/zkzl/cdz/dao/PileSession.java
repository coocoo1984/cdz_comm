package com.zkzl.cdz.dao;

import lombok.Data;

@Data
public class PileSession {
    private String id;
    private String code;
    private String sessionId;
    private String manufacturers;


}
