package com.zkzl.cdz.dao;

/**
 * @Description: 常量
 * @Author: jww
 * @CreateDate: 2019/8/10 14:43
 * @Version: 1.0.0
 */
public interface Constant {
    String redis_key = "cdz:";
    String redis_symbol = "&";
    Long redis_expire = 2 * 60 * 60L;

    interface gunState {
        String success = "success";
        String error = "error";
        String gunError = "gunError";
        String unKnow = "unKnow";
        String unConnect = "unConnect";
        String sendOK = "sendOK";
        String sendError = "sendError";
        String inUse = "inUse";
    }

    interface manufacturer {
        String YKR = "YKR";
    }

}
