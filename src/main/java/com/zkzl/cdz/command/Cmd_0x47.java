package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.dao.Order;
import com.zkzl.cdz.dao.SysDao;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.*;

/**
 * * @program: NettyTEST
 * <p>
 * * @description: 上传账单应答
 * * @author: QSY
 * * @create: 2019-07-22 11:57
 */
@Slf4j
@Component
public class Cmd_0x47 {

    @Autowired
    SysDao sysDao;

    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("收到上传账单数据_0x47 : " + Arrays.toString(cs.body));
        try {
            byte[] body = cs.body;
            byte[] number = new byte[22];  //流水号
            System.arraycopy(body, 0, number, 0, 22);
            String numberSTR = Tools.ASCIIConvertStr(number).trim();//充电流水号
            byte[] userCard = new byte[16];
            System.arraycopy(body, 22, userCard, 0, 16);
            String userCardSTR = Tools.ASCIIConvertStr(userCard).trim();//用户卡号
            byte[] FOUR = new byte[4];
            byte[] EIGHT = new byte[8];
            System.arraycopy(body, 38, FOUR, 0, 4);
            int currentPower = Tools.ByteConvertIntHL(FOUR);//本次充电电量
            System.arraycopy(body, 42, FOUR, 0, 4);
            int eFee = Tools.ByteConvertIntHL(FOUR);//电费总额
            System.arraycopy(body, 46, FOUR, 0, 4);
            int sFee = Tools.ByteConvertIntHL(FOUR);//服务费总额
            System.arraycopy(body, 50, EIGHT, 0, 8);

            //开始充电时间
            //byte[] startTime = EIGHT;
            int year = Tools.ByteConvertIntHL(EIGHT[7]) + 2000;
            int month = Tools.ByteConvertIntHL(EIGHT[6]) - 1;
            int day = Tools.ByteConvertIntHL(EIGHT[5]);
            int hour = Tools.ByteConvertIntHL(EIGHT[4]);
            int minute = Tools.ByteConvertIntHL(EIGHT[3]);
            int second = Tools.ByteConvertIntHL(EIGHT[2]);
            Calendar cld = Calendar.getInstance();
            cld.set(year, month, day, hour, minute, second);
            Date dateS = cld.getTime();//开始时间
            String dateSS = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateS);//开始时间
            Map<String, Object> map = new HashMap<>();//数据存储
            map.put("orderNo", numberSTR);
            map.put("createTime", dateSS);
            List<Order> orderList = sysDao.findOrderNoByMap(map);


            if (orderList != null && orderList.size() > 0) {//退款操作
                orderList.stream().forEach(a -> {
                    String orderNo = a.getOrderNo();
                    map.clear();
                    map.put("p1", orderNo);
                    sysDao.callErrorState3(map);
                });
            }

            //结束充电时间
            System.arraycopy(body, 58, EIGHT, 0, 8);
            int year2 = Tools.ByteConvertIntHL(EIGHT[7]) + 2000;
            int month2 = Tools.ByteConvertIntHL(EIGHT[6]) - 1;
            int day2 = Tools.ByteConvertIntHL(EIGHT[5]);
            int hour2 = Tools.ByteConvertIntHL(EIGHT[4]);
            int minute2 = Tools.ByteConvertIntHL(EIGHT[3]);
            int second2 = Tools.ByteConvertIntHL(EIGHT[2]);
            cld.set(year2, month2, day2, hour2, minute2, second2);
            Date dateE = cld.getTime();//结束时间
            String dateEE = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(dateE);//结束时间
            int endSOC = Tools.ByteConvertIntHL(body[66]);
            int reason = Tools.ByteConvertIntHL(body[67]);
            int duration = (int) ((dateE.getTime() - dateS.getTime()) / 1000 / 60);


            log.info("充值流水号：" + numberSTR +  "用户卡号：" + userCardSTR + "，本次充电电量：" +
                    currentPower + "，电费总额：" + eFee + "，服务费总额：" + sFee
                    +"，开始时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS" ).format(dateS)
                    +"，结束时间："+new SimpleDateFormat("yyyy-MM-dd HH:mm:ss SSS" ).format(dateE));
            map.clear();
            map.put("p1", numberSTR);
            map.put("p2", userCardSTR);
            map.put("p3", currentPower);
            map.put("p4", eFee);
            map.put("p5", sFee);
            map.put("p6", reason);
            map.put("p7", duration);
            map.put("p8", dateSS);
            map.put("p9", dateEE);
            sysDao.callAutoPile(map);

            if (map.get("result").toString().equals("1")) {
                log.info("账单数据成功存入数据库，并进行结算！");
                CmdStructure sendMess = new CmdStructure();
                sendMess.type = 0x48;
                sendMess.body = new byte[43];
                sendMess.body[0] = 0x00;
                System.arraycopy(number, 0, sendMess.body, 1, 22);
                System.arraycopy(userCard, 0, sendMess.body, 23, 16);
                sendMess.serial = cs.serial;
                ctx.write(sendMess.ToBytes(sendMess));
            } else {
                log.error("账单数据插入数据库失败！");
            }
        } catch (Exception e) {
            log.error("账单数据插入数据库异常", e);
        }
    }

}
