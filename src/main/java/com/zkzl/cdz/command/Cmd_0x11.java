package com.zkzl.cdz.command;

import com.alibaba.fastjson.JSON;
import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.dao.Constant;
import com.zkzl.cdz.dao.Pile;
import com.zkzl.cdz.netty.JRedisPool;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * * @program: NettyTEST
 * * @description: 获取枪状态应答
 * * @author: QSY
 * * @create: 2019-07-22 11:57
 */
@Slf4j
@Component
public class Cmd_0x11 {


    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("获取枪状态应答_0x11");
        try {
            Pile pile = new Pile();
            String sessionId = ctx.channel().id().toString();
            String gun = "1";//定义枪序号
            switch (cs.gun) {
                case 1:
                    gun = "1";
                    break;
                case 2:
                    gun = "2";
                    break;
                default:
                    break;
            }
            byte[] data = cs.body;
            byte[] Four = new byte[4];
            byte[] Two = new byte[2];
            System.arraycopy(data, 0, Four, 0, 4);
            int AllChargePower = Tools.ByteConvertIntHL(Four); //电量单位 kwh
            System.arraycopy(data, 4, Four, 0, 4);
            int AllWorkTime = Tools.ByteConvertIntHL(Four);
            System.arraycopy(data, 8, Four, 0, 4);
            int AllChargerTime = Tools.ByteConvertIntHL(Four);
            int macstate = data[12];
            int gunstate = data[13];
            int park = data[14];
            int lock = data[15];
            System.arraycopy(data, 16, Two, 0, 2);
            int BMSV = Tools.ByteConvertIntHL(Two);
            System.arraycopy(data, 18, Two, 0, 2);
            int BMSE = Tools.ByteConvertIntHL(Two);
            int BMSModel = data[20];
            System.arraycopy(data, 21, Two, 0, 2);
            int chargerV = Tools.ByteConvertIntHL(Two);//充电电压
            System.arraycopy(data, 23, Two, 0, 2);
            int chargerE = Tools.ByteConvertIntHL(Two);//充电电流
            System.arraycopy(data, 25, Four, 0, 4);
            int usedPower = Tools.ByteConvertIntHL(Four);//已充电量
            System.arraycopy(data, 29, Two, 0, 2);
            int usedTime = Tools.ByteConvertIntHL(Two);//已充时间
            System.arraycopy(data, 31, Two, 0, 2);
            int restTime = Tools.ByteConvertIntHL(Two);//估计剩余时间
            int SOC = Tools.ByteConvertIntHL(data[33]);
            System.arraycopy(data, 34, Two, 0, 2);
            int maxV = Tools.ByteConvertIntHL(Two);//最高单体蓄电池电压
            int maxVSN = Tools.ByteConvertIntHL(data[36]);
            System.arraycopy(data, 37, Two, 0, 2);
            int minV = Tools.ByteConvertIntHL(Two);//最低单体蓄电池电压
            int minVSN = Tools.ByteConvertIntHL(data[39]);
            int temperature = Tools.ByteConvertIntHL(data[40]);
            int point = Tools.ByteConvertIntHL(data[41]);
            System.arraycopy(data, 42, Four, 0, 4);
            int usedEFEE = Tools.ByteConvertIntHL(Four);//已充电费金额
            System.arraycopy(data, 46, Four, 0, 4);
            int usedSFEE = Tools.ByteConvertIntHL(Four);//已充服务费金额
            pile.setSessionId(sessionId);
            pile.setMacstate(macstate);//充电机状态
            pile.setGunstate(gunstate);//枪状态
            pile.setUsedPower(usedPower);//枪已充电量
            pile.setUsedTime(usedTime);//枪已充电时间
            //pile.remainingTime = Utils.ByteToIntHL(data.Skip(31).Take(2).ToArray());//枪估计剩余时间
            pile.setSoc(SOC);//soc
            pile.setEFee(usedEFEE);//当前电费 单位分
            pile.setSFee(usedSFEE);//当前服务费 单位分
            pile.setManufacturers(Constant.manufacturer.YKR);
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            pile.setDate(sdf.format(Calendar.getInstance().getTime()));
            String gunState = JSON.toJSONString(pile);
            String code_ = JRedisPool.getKV(Constant.redis_key + sessionId, 0);
            log.info("桩编号：" + code_ + "，枪：" + gun + ",状态：" + gunState);
            if (macstate == 2) {
                JRedisPool.setKV(code_ + cs.gun, Constant.gunState.success, 2);
            }
            JRedisPool.SetHashVal(sessionId, gun, gunState, 1, 90);
        } catch (Exception e) {
            log.error("异常_0x11", e);
        }
    }

}
