package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.CmdStructure;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Cmd_0x75 {


    /**
     * @Description: 对时应答
     * @Author: jww
     * @CreateDate: 2019/8/10 10:51
     * @Version: 1.0.0
     */

    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("收到对时应答_0x75");
    }

}
