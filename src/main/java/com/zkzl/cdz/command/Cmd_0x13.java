package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.dao.PileFailure;
import com.zkzl.cdz.dao.PileSession;
import com.zkzl.cdz.dao.SysDao;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

/**
 * @program: NettyTEST
 * @description: 故障应答
 * @author: QSY
 * @create: 2019-07-26 10:29
 **/
@Slf4j
@Component
public class Cmd_0x13 {

    @Autowired
    SysDao sysDao;

    @Transactional
    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        Map<Integer, String> map = new HashMap<>();
        for (int i = 0; i < cs.body.length; i++) {
            if (cs.body[i] == 1 & i == 0) {
                map.put(i + 1, "充电机故障总告警");
            } else if (cs.body[i] == 1 & i == 1) {
                map.put(i + 1, "急停按钮动作故障");
            } else if (cs.body[i] == 1 & i == 2) {
                map.put(i + 1, "无可用整流模块");
            } else if (cs.body[i] == 1 & i == 3) {
                map.put(i + 1, "充电机出风口温度过高");
            } else if (cs.body[i] == 1 & i == 4) {
                map.put(i + 1, "交流电压异常");
            } else if (cs.body[i] == 1 & i == 5) {
                map.put(i + 1, "交流防雷故障");
            } else if (cs.body[i] == 1 & i == 6) {
                map.put(i + 1, "交直流模块DC20通信中断");
            } else if (cs.body[i] == 1 & i == 7) {
                map.put(i + 1, "绝缘检测模块FC08通信中断");
            } else if (cs.body[i] == 1 & i == 8) {
                map.put(i + 1, "电度表通信中断");
            } else if (cs.body[i] == 1 & i == 9) {
                map.put(i + 1, "读卡器通信中断");
            } else if (cs.body[i] == 1 & i == 10) {
                map.put(i + 1, "RC10通信中断");
            } else if (cs.body[i] == 1 & i == 11) {
                map.put(i + 1, "风扇调速板故障");
            } else if (cs.body[i] == 1 & i == 12) {
                map.put(i + 1, "直流熔断器故障");
            } else if (cs.body[i] == 1 & i == 13) {
                map.put(i + 1, "高压接触器故障");
            } else if (cs.body[i] == 1 & i == 14) {
                map.put(i + 1, "充电机输出控制异常");
            } else if (cs.body[i] == 1 & i == 15) {
                map.put(i + 1, "充电机输出过压");
            } else if (cs.body[i] == 1 & i == 16) {
                map.put(i + 1, "充电机输出过流");
            } else if (cs.body[i] == 1 & i == 17) {
                map.put(i + 1, "急停按钮动作故障");
            } else if (cs.body[i] == 1 & i == 18) {
                map.put(i + 1, "充电机输出短路");
            } else if (cs.body[i] == 1 & i == 19) {
                map.put(i + 1, "门打开");
            }
        }
        if (map.size() == 0) {
            log.info("故障应答_0x13：无故障");
            return;
        }


        PileSession pileSession = new PileSession();
        pileSession.setSessionId(ctx.channel().id().toString());
        PileSession pileSession1 = sysDao.findPileSession(pileSession);

        String code = null;
        if (pileSession1 != null) {
            code = pileSession1.getCode();
        }
        if (code != null) {
            List<PileFailure> list = new ArrayList<>();
            Date now = new Date();
            StringBuilder sb = new StringBuilder();
            for (Integer ix : map.keySet()) {
                sb.append(map.get(ix) + "\t");
                PileFailure failure = new PileFailure();
                failure.setId(Tools.makeId());
                failure.setCode(code);
                failure.setCause(map.get(ix));
                failure.setCauseId(ix);
                failure.setTime(now);
                list.add(failure);
            }

            log.info("故障应答_0x13：" + sb.toString());
            sysDao.batchSavePileFailure(list);
        }

    }

}
