package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.netty.MangerChannel;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Cmd_0x12 {


    /**
     * * @program: NettyTEST
     * * @description: 发送获取实时状态指令
     * * @author: QSY
     * * @create: 2019-07-22 11:57
     */

    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("发送获取实时状态指令_0x12");
        String code = Tools.ASCIIConvertStr(cs.body);
        Channel channel = MangerChannel.getChannel(code);
        if (channel != null) {
            cs.body = null;//清空包体
            cs.type = 0x12;
            channel.writeAndFlush(cs.ToBytes(cs));
        }
        ctx.close();
    }

}
