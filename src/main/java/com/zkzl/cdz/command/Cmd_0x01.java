package com.zkzl.cdz.command;


import com.zkzl.cdz.dao.*;
import com.zkzl.cdz.netty.JRedisPool;
import com.zkzl.cdz.netty.MangerChannel;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

import java.util.Calendar;

/**
 * * @program: NettyTEST
 * * @description: 登录指令应答
 * * @author: QSY
 * * @create: 2019-07-22 11:57
 */
@Slf4j
@Component
public class Cmd_0x01 {
    @Autowired
    SysDao sysDao;


    @Transactional
    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        try {
            log.info("开始登录指令_0x01");
            String sessionId = ctx.channel().id().toString();
            byte[] body = cs.body;
            byte[] code = new byte[16];  //桩编码
            System.arraycopy(body, 0, code, 0, 16);
            String code_ = Tools.ASCIIConvertStr(code).trim();
            String id = Tools.makeId();
            String gunState = null;
            byte state = cs.body[33];
            if (state == 1) {
                gunState = "0";
            } else if (state == 2) {
                gunState = "0,0";
            } else if (state == 3) {
                gunState = "0,0,0";
            } else if (state == 4) {
                gunState = "0,0,0,0";
            }
            PileSession pileSession = new PileSession();
            pileSession.setId(id);
            pileSession.setCode(code_);
            pileSession.setSessionId(sessionId);
            pileSession.setManufacturers("YKR");
            sysDao.replacePileSession(pileSession);
            ChargingPile chargingPile = new ChargingPile();
            chargingPile.setState(gunState);
            chargingPile.setSn(code_);
            sysDao.flushChargingPile(chargingPile);
            MangerChannel.addChannel(code_, ctx.channel());//登录后进行管理已登录连接
            MangerChannel.addCode(ctx.channel(), code_);//登录后进行管理已登录连接
            JRedisPool.setKV(Constant.redis_key + sessionId, code_, 0);
            CmdStructure cs2 = new CmdStructure();
            cs2.type = 0x02;
            byte[] body2 = new byte[25];
            System.arraycopy(code, 0, body2, 1, 16);
            Calendar cld = Calendar.getInstance();
            int year = cld.get(Calendar.YEAR) - 2000;
            int month = cld.get(Calendar.MONTH) + 1;
            int day = cld.get(Calendar.DATE);
            int hour = cld.get(Calendar.HOUR_OF_DAY);
            int minute = cld.get(Calendar.MINUTE);
            int second = cld.get(Calendar.SECOND);
            int miss = cld.get(Calendar.MILLISECOND);
            body2[17] = Tools.IntConvertByteHL(miss, 2)[0];
            body2[18] = Tools.IntConvertByteHL(miss, 2)[1];
            body2[19] = (byte) minute;
            body2[20] = (byte) second;
            body2[21] = (byte) hour;
            body2[22] = (byte) day;
            body2[23] = (byte) month;
            body2[24] = (byte) year;
            cs2.body = body2;
            cs2.serial = cs.serial;
            cs2.gun = cs.gun;
            ctx.writeAndFlush(cs2.ToBytes(cs2));
        } catch (Exception e) {
            log.error("系统登陆失败", e);
        }
    }

}
