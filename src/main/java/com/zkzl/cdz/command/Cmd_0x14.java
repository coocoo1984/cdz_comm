package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.netty.MangerChannel;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

@Slf4j
@Component
public class Cmd_0x14 {

    /**
     * * @program: NettyTEST
     * * @description: 发送获取故障指令
     * * @author: QSY
     * * @create: 2019-07-22 11:57
     */
    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("发送获取故障指令_0x14");
        Channel channel = MangerChannel.getChannel("");
        if (channel != null) {
            channel.writeAndFlush(cs.ToBytes(cs));
        }
    }

}
