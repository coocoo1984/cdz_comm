package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Calendar;


/**
 * @program: NettyTEST
 * @description: 心跳包应答
 * @author: QSY
 * @create: 2019-07-22 11:57
 **/
@Slf4j
@Component
public class Cmd_0x03 {


    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("心跳包应答_0x03");
        CmdStructure cs2 = new CmdStructure();
        cs2.serial = cs.serial;
        cs2.type = 0x04;
        cs2.body = new byte[]{0x00};
        byte[] sendMess = cs2.ToBytes(cs2);
        ChannelFuture cf = ctx.writeAndFlush(sendMess);
        if (!cf.isDone()) {
            ctx.writeAndFlush(sendMess);
        }
        CmdStructure cs121 = new CmdStructure();
        cs121.serial = cs.serial;
        cs121.type = 0x12;
        cs121.gun = 0x01;
        byte[] byte1 = cs121.ToBytes(cs121);
        log.info("1号枪应答：" + Arrays.toString(byte1));
        ctx.writeAndFlush(byte1);
        CmdStructure cs122 = new CmdStructure();
        cs122.serial = cs.serial;
        cs122.type = 0x12;
        cs122.gun = 0x02;
        byte[] byte2 = cs122.ToBytes(cs122);
        log.info("2号枪应答：" + Arrays.toString(byte2));
        ctx.writeAndFlush(byte2);
    }


}
