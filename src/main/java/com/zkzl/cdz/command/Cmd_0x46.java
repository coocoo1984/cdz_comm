package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.netty.MangerChannel;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.Channel;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * * @program: NettyTEST
 * * @description: 发送停止充电指令
 * * @author: QSY
 * * @create: 2019-07-22 11:57
 */
@Slf4j
@Component
public class Cmd_0x46 {

    /**
     * 这里只需要解析sessionid即可
     *
     * @param ctx
     * @param cs
     */
    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("发送停止充电指令_0x46");
        byte[] chId = new byte[cs.body.length];
        System.arraycopy(cs.body, 0, chId, 0, cs.body.length);
        String channelId = Tools.ASCIIConvertStr(chId);
        Channel channel = MangerChannel.getChannel(channelId);
        if (channel != null) {
            cs.body = new byte[]{0x01};
            channel.writeAndFlush(cs.ToBytes(cs));
            log.warn("停止充电异常");
        }
    }

}
