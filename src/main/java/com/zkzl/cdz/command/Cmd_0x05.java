package com.zkzl.cdz.command;

import com.zkzl.cdz.dao.ChargeStation;
import com.zkzl.cdz.dao.SysDao;
import com.zkzl.cdz.dao.CmdStructure;
import com.zkzl.cdz.util.Tools;
import io.netty.channel.ChannelHandlerContext;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * @program: NettyTEST
 * @description: 获取计费模型指令应答
 * @author: QSY
 * @create: 2019-07-23 08:25
 **/
@Slf4j
@Component
public class Cmd_0x05 {

    @Autowired
    SysDao sysDao;

    public void Execute(ChannelHandlerContext ctx, CmdStructure cs) {
        log.info("获取计费模型指令应答_0x05");
        String sessionId = ctx.channel().id().toString();

        ChargeStation station = sysDao.findStationBySessionId(sessionId);

        try {
            if (station != null) {
                int sFee = station.getServicePrice();
                int eFee = station.getElectricFee()*1000;
                byte[] priceModel = new byte[67];
                //服务费
                byte[] sFee_bytes = Tools.IntConvertByteHL(sFee, 2);
                priceModel[0] = sFee_bytes[0];
                priceModel[1] = sFee_bytes[1];
                priceModel[2] = sFee_bytes[0];
                priceModel[3] = sFee_bytes[1];
                priceModel[4] = sFee_bytes[0];
                priceModel[5] = sFee_bytes[1];
                priceModel[6] = sFee_bytes[0];
                priceModel[7] = sFee_bytes[1];
                //电费
                byte[] eFee_bytes = Tools.IntConvertByteHL(eFee, 4);
                priceModel[8] = eFee_bytes[0];
                priceModel[9] = eFee_bytes[1];
                priceModel[10] = eFee_bytes[2];
                priceModel[11] = eFee_bytes[3];
                priceModel[12] = eFee_bytes[0];
                priceModel[13] = eFee_bytes[1];
                priceModel[14] = eFee_bytes[2];
                priceModel[15] = eFee_bytes[3];
                priceModel[16] = eFee_bytes[0];
                priceModel[17] = eFee_bytes[1];
                priceModel[18] = eFee_bytes[2];
                priceModel[19] = eFee_bytes[3];
                priceModel[20] = eFee_bytes[0];
                priceModel[21] = eFee_bytes[1];
                priceModel[22] = eFee_bytes[2];
                priceModel[23] = eFee_bytes[3];
                priceModel[24] = 0x01;//1段
                byte[] model_bytes = Tools.IntConvertByteHL(1440, 2);
                priceModel[25] = model_bytes[0];
                priceModel[26] = model_bytes[1];
                priceModel[27] = 0x01;//标志1
                cs.body = priceModel;
                cs.type = 0x06;
                byte[] message = cs.ToBytes(cs);
                ctx.writeAndFlush(message);
            }
        } catch (Exception e) {
            log.error("无session对应的站点_0x05", e);
        }
    }

}
